﻿namespace AsposeWords.Data
{
    public class PathSettings
    {
        public string DirectoryPathFile { get; set; }
        
        public string DirectoryPathImg { get; set; }
        
        public string FileName { get; set; }
        
    }
}