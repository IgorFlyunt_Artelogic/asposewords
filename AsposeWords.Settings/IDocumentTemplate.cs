﻿using Aspose.Words;

namespace AsposeWords.Data
{
    public interface IDocumentTemplate
    {
        public Document Render();

        public void Save(Document document, PathSettings pathSettings, SaveFormat format);
    }
}