﻿using Aspose.Words;
using AsposeWords.Data;
using AsposeWords.Templates;


namespace AsposeWords.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = new PathSettings()
            {
                DirectoryPathFile = @"C:\Projects\asposewords\doc",
                DirectoryPathImg = @"C:\Projects\asposewords\img",
                FileName = "SimpleSalesDocument.docx"
            };
            
            IDocumentTemplate template = new SimpleSalesTemplate(path);
            var document = template.Render();
            template.Save(document, path, SaveFormat.Docx);
        }
    }
}