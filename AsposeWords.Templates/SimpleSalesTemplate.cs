﻿using System.Collections.Generic;
using Aspose.Words;
using AsposeWords.Data;
using AsposeWords.Utilities;

namespace AsposeWords.Templates
{
    public class SimpleSalesTemplate : IDocumentTemplate
    {
        private PathSettings PathSettings { get; set; }

        public SimpleSalesTemplate(PathSettings pathSettings)
        {
            PathSettings = pathSettings;
        }


        public Document Render()
        {
            var document = new Document();
            var builder = new DocumentBuilder(document);
            var salesModel = new SalesModel();
            var fontFormatter = new TextFormatter(builder);
            var paragraphFormatter = new ParagraphFormatter(builder);
            var imageInserter = new ImageInserter(builder, PathSettings);
            var tableCreator = new TableCreator(builder, PathSettings, imageInserter);

            FormatFontForHeader(fontFormatter);
            FormatParagraphForHeader(paragraphFormatter);
            HeaderText(builder);


            FormatFontForSalesTable(fontFormatter);
            FormatParagraphForSalesTable(paragraphFormatter);
            SalesTable(tableCreator);
            
            FormatFontForSalesArticle(fontFormatter);
            FormatParagraphForSalesArticle(paragraphFormatter);
            SalesArticle(builder, salesModel);
            
            SalesDashboardImage(imageInserter);
            

            return document;
        }

        

        #region Header

        private void FormatFontForHeader(TextFormatter textFormatter)
        {
            textFormatter.ApplyStyle(DocumentObjectType.Header);
        }

        private void FormatParagraphForHeader(ParagraphFormatter paragraphFormatter)
        {
            paragraphFormatter.ApplyStyle(DocumentObjectType.Header);
        }

        private void HeaderText(DocumentBuilder builder)
        {
            builder.Writeln("Sales Report");
        }

        #endregion


        #region SalesTable

        private void FormatFontForSalesTable(TextFormatter textFormatter)
        {
            textFormatter.ApplyStyle(DocumentObjectType.Table);
        }

        private void FormatParagraphForSalesTable(ParagraphFormatter paragraphFormatter)
        {
            paragraphFormatter.ApplyStyle(DocumentObjectType.Table);
        }


        private void SalesTable(TableCreator tableCreator)
        {
            var model = new SalesModel();
            
            tableCreator.CreateHeaderRow(new List<string>()
            {
                "Purchase_number",
                "Date_of_ purchase",
                "Customer_id",
                "Item_code"
            });

            tableCreator.CreateRow(new List<string>()
            {
                "1",
                "01.10.2021",
                "1213",
                "A_1",
            });

            tableCreator.CreateRow(new List<string>()
            {
                "2",
                "02.10.2021",
                "3765",
                "C_2",
            });

            tableCreator.CreateRow(new List<string>()
            {
                "3",
                "03.10.2021 ",
                "1853",
                "D_1",
            });

            tableCreator.CreateRow(new List<string>()
            {
                "4",
                "04.10.2021 ",
                "1853",
                "D_1",
            });

            tableCreator.CreateRow(new List<string>()
            {
                "6",
                "05.10.2021 ",
                "5692",
                "B_1",
            });
            

            foreach (var cellText in model.CellsList())
            {
                tableCreator.CreateCell(cellText);
            }
            
            
            tableCreator.EndTable();
        }

        #endregion
        
        
      
        
        private void FormatParagraphForSalesArticle(ParagraphFormatter paragraphFormatter)
        {
            paragraphFormatter.ApplyStyle(DocumentObjectType.Article);
        }
        
        private void FormatFontForSalesArticle(TextFormatter textFormatter)
        {
            textFormatter.ApplyStyle(DocumentObjectType.Article);
        }
        
        private void SalesArticle(DocumentBuilder builder, SalesModel salesModel)
        {
            builder.Writeln(salesModel.SalesArticle());
            
            //TODO: same with Paragraph
        }
        
        
        //TODO:move image inserter in that method
        private void SalesDashboardImage(ImageInserter imageInserter)
        {
            imageInserter.InsertImg("salesDashboard.jpg");
        }


        public void Save(Document document, PathSettings pathSettings, SaveFormat format)
        {
            document.Save(pathSettings.DirectoryPathFile + pathSettings.FileName, format);
        }
        
    }
}