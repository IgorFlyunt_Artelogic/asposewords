﻿using System.Collections.Generic;

namespace AsposeWords.Templates
{
    public class SalesModel
    {
        public List<string> CellsList()
        {
            return new List<string>()
            {
                "6",
                "06.10.2021",
                "4479",
                "A_1",
            };
        }

        public string SalesArticle()
        {
            return "Sales metrics are the key performance indicators, or KPIs, that empower a salesperson," +
                   " team or organization to assess performance against goals and objectives, monitor progress" +
                   " and make necessary adjustments for continued sales success.";
        }
    }
}