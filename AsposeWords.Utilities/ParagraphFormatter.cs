﻿using Aspose.Words;

namespace AsposeWords.Utilities
{
    public class ParagraphFormatter : BaseFormatter
    {
        private readonly DocumentBuilder _builder;

        public ParagraphFormatter(DocumentBuilder builder)
        {
            _builder = builder;
        }

        protected override void HeaderStyle()
        {
            _builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
        }

        protected override void TableStyle()
        {
            _builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
        }


        protected override void ArticleStyle()
        {
            _builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            _builder.ParagraphFormat.SpaceBefore = 20;
            _builder.ParagraphFormat.SpaceAfter = 50;
        }
        

        protected void CustomParagraph()
        {
            ParagraphFormat paragraphFormat = _builder.ParagraphFormat;
            paragraphFormat.FirstLineIndent = 8;
            paragraphFormat.Alignment = ParagraphAlignment.Justify;
            paragraphFormat.KeepTogether = true;
        }
    }
}