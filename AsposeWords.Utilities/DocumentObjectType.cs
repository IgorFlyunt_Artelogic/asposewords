﻿namespace AsposeWords.Utilities
{
    public enum DocumentObjectType
    {
        Header,
        Article,
        Table
    }
}