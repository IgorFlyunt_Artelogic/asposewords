﻿using System.Collections.Generic;
using System.Drawing;
using Aspose.Words;
using Aspose.Words.Tables;
using AsposeWords.Data;

namespace AsposeWords.Utilities
{
    public class TableCreator
    {
        private readonly DocumentBuilder _builder;
        private readonly PathSettings _pathSettings;
        private readonly ImageInserter _imageInserter;

        public TableCreator(DocumentBuilder builder, PathSettings pathSettings, ImageInserter imageInserter)
        {
            _builder = builder;
            _pathSettings = pathSettings;
            _imageInserter = imageInserter;
        }
        
        public void CreateHeaderRow(List<string> rowsName)
        {
            Table table = _builder.StartTable();
            
            

            foreach (var row in rowsName)
            {
                _builder.InsertCell();
                _builder.Write(row);

                //Apply formatting for the header rows
                table.PreferredWidth = PreferredWidth.FromPercent(100);
                _builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(10);
                _builder.CellFormat.Borders.LineWidth = 1.5;
                _builder.CellFormat.Shading.BackgroundPatternColor = Color.SteelBlue;
                _builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            }
            

            _builder.EndRow();
        }
        
        public void CreateRow(List<string> cells)
        {

            foreach (var cell in cells)
            {
                _builder.InsertCell();
                _builder.Write(cell);
                
                ////Apply formatting for the all cells
                _builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                _builder.CellFormat.VerticalMerge = CellMerge.First;
                _builder.CellFormat.Borders.LineWidth = 1;
                _builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
            }

            _builder.EndRow();
        }
        
        public void CreateCell(string text)
        {
            _builder.InsertCell();
            
            if (!string.IsNullOrEmpty(text))
            {
                _builder.Write(text);
            }
        }

        public void EndTable()
        {
            _builder.EndTable();
        }
        
        //TODO:Merge Cells
        
    }
}