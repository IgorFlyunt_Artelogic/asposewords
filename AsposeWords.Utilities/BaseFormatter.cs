﻿namespace AsposeWords.Utilities
{
    public abstract class BaseFormatter
    {

        public void ApplyStyle(DocumentObjectType objectType)
        {
            switch (objectType)
            {
                case DocumentObjectType.Header: HeaderStyle();
                    break;
                case DocumentObjectType.Table: TableStyle();
                    break;
                case DocumentObjectType.Article: ArticleStyle();
                    break;
            }
        }

        protected abstract void HeaderStyle();

        protected abstract void TableStyle();

        protected abstract void ArticleStyle();
        
    }
}