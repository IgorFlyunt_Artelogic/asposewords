﻿using System.Drawing;
using Aspose.Words;

namespace AsposeWords.Utilities
{
    public class TextFormatter : BaseFormatter
    {
        private readonly DocumentBuilder _builder;


        public TextFormatter(DocumentBuilder builder)
        {
            _builder = builder;
        }
        

        protected override void HeaderStyle()
        {
            _builder.Font.Name = "Calibri";
            _builder.Font.Size = 24;
            _builder.Font.Bold = true;
        }

        protected override void TableStyle()
        {
            _builder.Font.Name = "Arial";
            _builder.Font.Size = 12;
        }

        protected override void ArticleStyle()
        {
            _builder.Font.Name = "Times New Roman";
            _builder.Font.Size = 14;
            _builder.Bold = false;
        }
        
        protected void CustomFont(double size,
            string name = "Arial",
            bool bold = false,
            bool italic = false)
        {
            Font font = _builder.Font;
            font.Size = size;
            font.Name = name;
            font.Bold = bold;
            font.Italic = italic;
        }
    }
}