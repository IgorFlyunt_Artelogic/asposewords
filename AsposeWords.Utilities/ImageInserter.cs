﻿using Aspose.Words;
using Aspose.Words.Drawing;
using AsposeWords.Data;

namespace AsposeWords.Utilities
{
    /// <summary>
    /// All images needs to contain in DirectoryPathImg specify in AsposeWords.Settings/Data.cs
    /// </summary>
    public class ImageInserter
    {
        private readonly PathSettings _pathSettings;
        private readonly DocumentBuilder _builder;

        public ImageInserter(DocumentBuilder builder, PathSettings pathSettings)
        {
            _builder = builder;
            _pathSettings = pathSettings;
        }


        public void InsertImg(
            string imgName,
            double imgWidth = 300,
            double imgHeight = 300)
        {
            _builder.InsertImage((_pathSettings.DirectoryPathImg + imgName), imgWidth, imgHeight);
        }


        public void InsertAbsoluteImg(
            string imgName,
            double imgWidth = 100,
            double imgHeight = 100,
            double marginLeft = 100,
            double marginTop = 100)
        {
            _builder.InsertImage(_pathSettings.DirectoryPathImg + imgName,
                RelativeHorizontalPosition.Margin, marginLeft,
                RelativeVerticalPosition.Margin, marginTop, imgWidth, imgHeight, WrapType.Square);
        }
    }
}